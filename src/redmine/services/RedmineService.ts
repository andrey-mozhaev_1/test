import redmine from "../../core/loaders/redmine";
import {types} from "util";
import isSet = module;

export default class RedmineService {
    redmine: any;

    constructor() {
        this.redmine = redmine();
    }

    async getIssues() {
        const issues = await this.redmine.issues();

        return issues.data;
    }

    async getIssuesById(id: number) {
        const issues = await this.redmine.get_issue_by_id(id);

        return issues.data;
    }

    async createIssues(params) {
        const newIssues = await this.redmine.create_issue(params);

        return newIssues;
    }

    async updateIssues(id, params) {
        const issues = await this.redmine.update_issue(id, params);

        return issues;
    }

    async deleteIssues(id) {
        const isDelete = await this.redmine.delete_issue(id);

        return isDelete;
    }
}
