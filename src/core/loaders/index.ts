import expressLoader from "./express";
import express from "express";
import Logger from "../logger";

export default async (expressApp: express.Application): Promise<void> => {
    try{
        await expressLoader({app: expressApp});
    } catch (e) {
        Logger.error(e);
    }
};
