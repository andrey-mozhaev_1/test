import {json} from "express";

const Redmine = require("axios-redmine");

export default () => {
    const hostname = process.env.REDMINE_HOST;
    const config = {
        apiKey: process.env.API_KEY,
        format: "json",
    };

    return new Redmine(hostname, config);
};

