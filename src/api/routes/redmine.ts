import {Router, Request, Response} from "express";
import RedmineService from "../../redmine/services/RedmineService";
import Logger from "../../core/logger";

const route = Router();
const redmineService = new RedmineService();

export default (app: Router): void => {
    app.use("/api/redmine", route);

    route.get("/", async (req: Request, res: Response): Promise<void> => {
        try {
            const issues = await redmineService.getIssues();

            res.status(200).json(issues);
        } catch (e) {
            res.status(500).json("server error");
            Logger.error(e);
        }
    });

    route.get("/:id", async (req: Request, res: Response): Promise<void> => {
        try {
            const { id } = req.params;
            const issues = await redmineService.getIssuesById(+id);

            res.status(200).json(issues);
        } catch (e) {
            const { id } = req.params;
            res.status(404).send(`issues with id = ${id} not exist`);
            Logger.error(e);
        }
    });

    route.post("/", async (req: Request, res: Response): Promise<void> => {
        try {
            const issues = await redmineService.createIssues({issue: req.body});

            res.status(200).json(issues);
        } catch (e) {
            res.status(500).send("server error");
            Logger.error(e);
        }
    });

    route.patch("/:id", async (req: Request, res: Response): Promise<void> => {
        try {
            const { id } = req.params;
            const issues = await redmineService.updateIssues(id, {issue: req.body});

            res.status(200).json(issues);
        } catch (e) {
            res.status(500).send("server error");
            Logger.error(e);
        }
    });

    route.delete("/:id", async (req: Request, res: Response): Promise<void> => {
        try {
            const { id } = req.params;
            const isDelete = await redmineService.deleteIssues(id);

            res.status(200).json(isDelete);
        } catch (e) {
            res.status(500).send("server error");
            Logger.error(e);
        }
    });
};
